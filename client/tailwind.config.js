const colors = require('tailwindcss/colors')
module.exports = {
  purge: {
    content: [
      './src/**/*.js'
    ],
    enabled: true,
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        mono: ['"Space Mono"', 'mono']
      }
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
      gray: {
        DEFAULT: '#dedede'      
      },
      green: {
        DEFAULT: '#82ff82'       
      },
      pink: {
        DEFAULT: '#e8d6f5'    
      },
      smoke: {
        DEFAULT: '#738273'       
      }
    }
  },
  variants: {
    extend: {
      backgroundColor: ['disabled']
    },
  },
  plugins: [],
}
