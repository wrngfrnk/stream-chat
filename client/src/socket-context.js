import { createContext } from 'preact'
import io from 'socket.io-client'

const SocketContext = createContext({
  socket: null,
  connect: () => {},
  disconnect: () => {}
})

export default SocketContext
