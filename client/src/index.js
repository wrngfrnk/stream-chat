import { useState, useEffect } from 'preact/hooks'
import SocketContext from './socket-context'
import io from 'socket.io-client'

import Chat from './components/Chat'
import Join from './components/Join'

import './styles/main.css'

const API_URL = process.env.PREACT_APP_API_URL
const CHAT_NAMESPACE = process.env.PREACT_APP_CHAT_NAMESPACE

console.log(process.env)
export default function App() {
  const [name, setName] =       useState(sessionStorage.getItem('name') || '')
  const [token, setToken] =     useState(sessionStorage.getItem('token') || null)
  const [error, setError] =     useState(false)
  const [loading, setLoading] = useState(true)  
  const [socket, setSocket] =   useState(null)
  
  const connect = () => {
    setSocket(io.connect(`${API_URL}/${CHAT_NAMESPACE}`))
  }

  const disconnect = () => {
    socket.disconnect()
    setSocket(null)
  }

  useEffect(() => {
    const ping = async() => {
      try {
        let response = await fetch(`${API_URL}`, {
          method: 'GET',
          mode: 'no-cors'
        }) 
        
        let result = await response.text()
        if(result.ok) return true
      } catch(e) {
        console.error('Connection to chat server failed:', e)
        setError('Chat offline')
      }
    }
    
    ping() // TODO: Purify. It's like this for now because handling failed fetch promises sucks.
    setLoading(false)
  }, [])

  const login = async(name, password) => {
    let user = { name, password }

    const response = await fetch(`${API_URL}/auth`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user)
    })

    let result = await response.json()
  
    if(result.success) {
      setName(result.name)
      sessionStorage.setItem('name', result.name)
    
      if(result.token) {
        setToken(result.token)
        sessionStorage.setItem('token', result.token)
      }

    } else {
      alert(result.error)
    } 
  } 

  // TODO: fix ambiguous naming, disconnect() handles actually leaving the chat.
  const leaveChat = () => {
    setName('')
    sessionStorage.removeItem('name')
    
    setToken(null)
    sessionStorage.removeItem('token')
  }
 
  if(error)   return <span>{error}</span>
  if(loading) return <span>Loading...</span>
	if(!name)   return <Join onJoin={login} />

  return (
    <div className='h-screen'>
      <SocketContext.Provider value={{ socket, connect, disconnect }}>
        <Chat name={name} onLogOut={leaveChat} token={token} />
      </SocketContext.Provider>
    </div>
  )
}
