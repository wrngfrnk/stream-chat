import { memo } from 'preact/compat'

// memoized to prevent the message from resetting every time the parent state updates.
const ChatInput = ({ onSendMessage, setMessage, message }) => {

	return (
		<form className='flex flex-row'>
			<input 
			  value={message} 
			  placeholder="type a message here..." 
			  type='text' 
			  onChange={e => setMessage(e.target.value)} 
				maxLength='1024'
				className='w-full px-2 py-1 rounded-tl rounded-bl'
			/>
			<button 
				className='px-2 py-1 font-mono rounded-tr rounded-br bg-green flex-end'
				onClick={e => onSendMessage(e)}
			>
				Send
			</button>
		</form>
	)
}

export default memo(ChatInput)
