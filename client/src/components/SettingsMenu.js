import { useState } from 'preact/hooks'

export default function SettingsMenu({ changeName, logOut }) {
	const [open, setOpen] = useState(false)
  
	return (
		<>
			<input type='button' onClick={() => setOpen(!open)} value='⚙️' />
			{open && (
				<div>
					<button onClick={logOut} class="font-mono">Leave chat</button>
				</div>
			)}
		</>
	)
}
