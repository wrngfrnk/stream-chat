import { useState, useContext } from "preact/hooks"
import SocketContext from "../socket-context"

export default function MessageActions({ id, flag }) {
  const [show, setShow] = useState(false)
  const { socket } = useContext(SocketContext)

  const deleteMessage = () => {
    socket.emit('deleteMessage', id)
    setShow(false)
  }
  
  // TODO: Could be a little bit more robust.
  // But we validate this server-side so I guess this is OK for now. Maybe. Hopefully.
  if(!sessionStorage.getItem('token')) return null 

  if(!show) return (
    <button onClick={() => setShow(true)} alt='Delete message' className='ml-1'>
      ×
    </button>
  )

  return (
    <span className='ml-1 text-xs text-red-700 space-x-2'>
      <span>delete?</span>
      <button onClick={deleteMessage} className='px-1 rounded bg-green'>yes</button>
      <button onClick={() => setShow(false)} className='px-1 rounded bg-pink'>no</button>
    </span>
  )
} 
