import { useEffect, useRef } from 'preact/hooks'
import Message from './Message'

export default function MessageHistory({messages}) {
	const messagesEndRef = useRef(null)
	const scrollToBottom = () => {
		messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' })
	}

	useEffect(() => {
    scrollToBottom()
	}, [messages])

	return (
	  <div>
			{ messages.map(v => (
				<Message {...v} key={v.id} />
			) )}  
			<div ref={messagesEndRef} />
	  </div>
	)
}
