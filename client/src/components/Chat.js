import { useEffect, useState, useContext, useCallback } from 'preact/hooks'
import SocketContext from './../socket-context'

import MessageHistory from './MessageHistory'
import ChatInput from './ChatInput'
import UserList from './UserList'

export default function Chat({ name, token, onLogOut }) {
	const [joined, setJoined] = useState(false)
	const [users, setUsers] = useState([])
	const [message, setMessage] = useState('')
	const [messageHistory, setMessageHistory] = useState([])
  const [showMenu, setShowMenu] = useState(false)
  const { socket, connect, disconnect } = useContext(SocketContext)

	// TODO: This kind of sucks
	useEffect(() => {
    connect()
	}, [])

  useEffect(() => { 
		const addSocketListeners = () => {
			if(socket) {
				socket.emit('join', { name, token }, err => {
					if(err) {
						alert(err) // TODO: Something less intrusive maybe
						logOut()
					} else {
						setJoined(true)
					}
				})

				socket.on('message', msg => {
					setMessageHistory(messageHistory => [...messageHistory, msg])
				})

				socket.on('deleteMessage', id => {
					setMessageHistory(messageHistory => messageHistory.filter(msg => msg.id !== id))
				})

				socket.on('chatData', ({ users }) => {
					setUsers(users)
				})

				socket.on('connect_error', (e) => {
					alert('Connection to chat was lost!')
					console.error(e)
					logOut()
				})

				socket.on('force_leave', (reason) => {
					alert(`Removed from chat: ${reason}`)
					logOut()
				})
			}
		}
    addSocketListeners()
  }, [socket])
 
	const sendMessage = (e) => {
		e.preventDefault()
		if(message) {
			socket.emit('sendMessage', message, () => setMessage(''))
		}
	}

	const logOut = () => {
		disconnect()
		socket.disconnect()
	  onLogOut()
	}

	const toggleMenu = () => {
		setShowMenu(!showMenu)
	}
  
	if(joined) return (
		<div className='flex flex-col w-full h-full'>
				<button onClick={toggleMenu} 
					className="w-24 pb-1 mx-auto text-xl text-white rounded-bl rounded-br bg-pink flex-start "
				>
					☰
				</button>

			<div className={`h-3/4 w-full right-0 flex flex-col bg-white border-b shadow-lg ${showMenu ? 'absolute' : 'hidden'}`}>
				<button onClick={toggleMenu} className='w-24 pb-1 mx-auto text-xl text-white rounded-bl rounded-br bg-pink flex-start'>
					× 
				</button>
				<div className='flex flex-col w-full h-full'>
					<div className='w-full h-full px-2 overflow-y-auto flex-start'>
						<UserList users={users} />
					</div>

					<button onClick={logOut} className='px-4 py-2 mx-3 my-2 font-mono rounded bg-pink flex-end'>
						Leave chat
					</button>
				</div>
			</div>
  		
			<div className='flex flex-col w-full h-full border-r'>
				<div className='h-full px-1 overflow-y-auto'>
					<MessageHistory messages={messageHistory} />
				</div>
				<div className='w-full h-auto px-2 py-2 border-t bg-gray border-gray flex-end'>
					<ChatInput onSendMessage={sendMessage} setMessage={setMessage} message={message} />
				</div>
			</div>
		</div>
	)

	return <span>Joining chat...</span>
}
