export default function Rules({ onAccept }) {
  return (
	  <div className="mb-2 text-sm text-center align-middle">
			<h3 className="mb-4 font-mono font-semibold">Chatroom rules</h3>
			<div>
				<p>Welcome to the chatroom! Here are simple some rules to follow:</p>
				<ul className="pb-3 my-2 ml-5 text-left list-disc border-b">
					<li className="list-disc">We are an inclusive organisation and are open for everyone. Please be considerate.</li>
					<li className="list-disc">No racism, sexism, homophobia, or other forms of discrimination</li>
					<li className="list-disc">Do not harrass, verbally assault, or act rudely towards others.</li>
					<li className="list-disc">Do not share harmful, offensive, or illegal content.</li>
					<li className="list-disc">Use common sense and have a good time!</li>
				</ul>
				<p className="mb-3 font-mono font-semibold">Privacy notice</p>
				<ul className="pb-3 ml-5 text-left list-disc border-b">
					<li>
						This chat application stores your username in the browser's SessionStorage, which is used to log you in automatically on recurring visits. The cookie is cleared when your browser session ends.
					</li>
					<li>
						The chat server stores a log of all messages sent, when and by whom. 
					</li>
					<li>
						The server stores the username, IP, and session ID for every user in it's local memory. The user data is created as soon as a user joins, and deleted when they leave. 
					</li>
					<li>
					The IP and session ID are not logged in any way, unless a user is banned, in which case only the IP will be saved to a blacklist.
					</li>
				</ul>
				<p className="my-5">If all of this seems fair to you, you're more than welcome to join. Enjoy!</p>
				<button className="px-4 py-1 font-mono text-lg rounded bg-pink" onClick={onAccept}>
					I accept!
				</button>
			</div>
		</div>
  )
}
