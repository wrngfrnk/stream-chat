import dayjs from 'dayjs'

import MessageActions from './MessageActions'

const Timestamp = ({time}) => {
	return (
		<span className='text-xs italic text-gray'>
		  { dayjs(time).format('HH:mm:ss') }
		</span>
	)
}

export default function Message({ id, user, text, time, flag }) {
	// flags: 0 = normal message, 1 = notification
	
	if(flag === 1) {
		return (
		  <div className='text-sm italic text-gray'>
				<span>{text}</span>
				<div className='float-right'>
					<Timestamp time={time} />
					<MessageActions id={id} flag={flag} />
				</div>
			</div>
		)
	}

	return (
    <div className='text-sm'>
			<span className='font-mono text-smoke'>&lt;{user}&gt;&nbsp;</span>
			<span className='break-all'>{text}</span>
			<div className='float-right'>
  			<Timestamp time={time} />
			  <MessageActions id={id} flag={flag} />
			</div>
		</div>
	)
} 
