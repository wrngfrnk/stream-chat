import { useState, useContext } from  'preact/hooks'
import SocketContext from '../socket-context'

// TODO: Uses a bunch of logic from ./MessageActions, maybe reuse that? DRY and all that.
// TODO: fix bracket hell on the bottom of this file.
export default function UserList({ users }) {
	const [show, setShow] = useState(false)
	const { socket } = useContext(SocketContext)
	
	const banUser = (name) => {
    socket.emit('banUser', name) 
	}

	const hasToken = sessionStorage.getItem('token') // Extremely rudimentary permission check. Validated serverside.

	return (
		<div>
			<p className="text-sm">{users.length} user{users.length !== 1 ? 's' : ''} online</p>
			<ul className="font-mono text-sm text-blue-700">
				{ users.map(v => (
				  <li key={v.name}>
					  ∙ {v.name}{(v.flag === 1 && <span> ☆</span>)}
						
					  { (hasToken && !show) && (
						  <button onClick={() => setShow(true)} alt='Ban user' className='ml-1'>
								×
              </button>
						) }
						
					  { (hasToken && show) && (
							<span className='ml-1 text-xs text-red-700 space-x-2'>
								<span>ban?</span>
								<button onClick={() => banUser(v.name)} className='px-1 rounded bg-green'>
								  yes
								</button>
                <button onClick={() => setShow(false)} className='px-1 rounded bg-pink'>
								  no
								</button>
							</span>
						) }
					</li>
				)) }
  		</ul>
  	</div>
	)
}
