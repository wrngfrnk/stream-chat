import { useState, useEffect } from 'preact/hooks'
import Rules from './Rules'

export default function Join({onJoin}) {
  const [name, setName] = useState('')
  const [isAdmin, setIsAdmin] = useState(false)
  const [password, setPassword] = useState('')
  const [rulesAccepted, setRulesAccepted] = useState(sessionStorage.getItem('rulesAccepted') || false) 
  const [showAdminLogin, setShowAdminLogin] = useState(false)

  // Show admin login if M is held for a while
  // This is not so much for security as it is for
  // preventing end users from getting confused.
  // "What password? How do I register? Am I the moderator??"
  // You know the type.
  useEffect(() => {
    let timeout 
    
    // JS needs the function to clear it from a listener because JS.
    const startTimeout = () => { 
      timeout = setTimeout(() => {
        setShowAdminLogin(!showAdminLogin)
      }, 500)
    }

    const stopTimeout = () => {
      clearTimeout(timeout)
    }

    window.addEventListener('keydown', (evt) => {
      if(evt.keyCode === 77) {
        startTimeout()
      }
    })

    window.addEventListener('keyup', (evt) => {
      if(evt.keyCode === 77) {
        stopTimeout()
      }
    })

    return(() => {
      window.removeEventListener('keydown', startTimeout)
      window.removeEventListener('keyup', stopTimeout)
    })
  }, [])

  const toggleAdmin = (e) => {
    setIsAdmin(e.target.value)
  }

  const onSubmit = (e) => {
    e.preventDefault()
    onJoin(name, password)
  }

  const acceptRules = () => {
    setRulesAccepted(true)
    sessionStorage.setItem('rulesAccepted', true)
  }
  
  if(!rulesAccepted) {
    return <Rules onAccept={acceptRules} />
  }

  // TODO: stop mixing ' and " pls i beg of me
  return (
    <div className="mx-auto">
      <div className="px-2 font-mono text-center">
        <p>Enter a username to join the chat.</p>
        <p className="text-xs">Usernames can contain letters, numbers, _ and -</p>
      </div>
      
      <form className="px-2" onSubmit={onSubmit}>
        <div>
          <label for='username' className='text-xs'>
            Username  
          </label>
          <input 
            type='text' 
            name='username'
            onChange={e => setName(e.target.value)} 
            placeholder="Enter username..." 
            maxLength='16'
            className="block px-3 py-1 border rounded-lg bg-gray-50"
          /> 
        </div>
        
        { isAdmin && (
          <div className="mt-1">
            <label for='password' className="text-sm">
              Password
            </label>
            <input 
              type='password' 
              value={password} 
              onChange={e => setPassword(e.target.value)} 
              placeholder="Enter password..." 
              className="block px-3 py-1 border rounded-lg bg-gray-50"
            />
          </div>
        ) }
       
        { showAdminLogin && (
          <div className="my-1">
            <input 
              type='checkbox' 
              onChange={e => setIsAdmin(e.target.checked)} 
            /> 
            <label for='admin'> Log in as chat moderator</label>
         </div> 
        )}

        <input
          type='submit' 
          value='Join chat' 
          className='w-full px-8 py-2 mx-auto my-2 font-mono rounded cursor-pointer bg-green disabled:bg-gray'
        />
      </form>
    </div>
  )
}

