import envVars from 'preact-cli-plugin-env-vars'

export default {
  webpack(config, env, helpers) {
    config.output.publicPath = './'  
    envVars(config, env, helpers)
  }
  
}

