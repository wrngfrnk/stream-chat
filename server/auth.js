const crypto = require('crypto')
// should have just used JWT... oh well.


// This could probably be done clientside.
const hash = (string) => {
  return crypto.createHash('sha256').update(string).digest('hex')
}

const salt = (string) => {
  // TODO: This.
  // it really isn't necessary for a project this size, but would be cool nonetheless.
  return null
}

const authenticate = (password) => {
	return hash(password) === (process.env.ADMIN_PASSWORD_HASH || hash('hackmeplz'))
}

const generateToken = () => {
  return crypto.randomBytes(64).toString('hex')
}

module.exports = { hash, authenticate, generateToken }
