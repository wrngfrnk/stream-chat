require('dotenv').config()

const https = require('https')
const http = require('http')
const express = require('express')
const bodyparser = require('body-parser')
const socketio = require('socket.io')
const crypto = require('crypto')
const cors = require('cors')
const fs = require('fs')

const { hash, authenticate, generateToken } = require('./auth')
const { addUser, verifyUser, removeUser, getUser, getUserByName, getUsersInChat, banUser  } = require('./users');

const app = express()
let server
if(process.env.HTTPS) {
	server = https.createServer({
		 key: fs.readFileSync(process.env.PRIVKEY_FILE),
		 cert: fs.readFileSync(process.env.FULLCHAIN_FILE)
	}, app)
} else {
	server = http.createServer(app)
}

const io = socketio(server, { cors: true, origins: ['*'] }) // TODO: localhost or production server only
const chatroom = io.of(process.env.CHATROOM_NAMESPACE || 'chat')

app.use(cors()) // heck CORS, all my homies hate CORS.
app.use(bodyparser.urlencoded({ extended: true  }))

app.get('/', (req, res) => {
	res.send('Chat server operational. Nothing to see here.')
})

app.get('/users', (req, res) => {
	res.send(getUsersInChat())
})

app.post('/auth', bodyparser(), (req, res) => {
	const { name, password } = req.body
	if(password) {
		if(authenticate(password)) {
			const token = generateToken()
			verifyUser(name, token)
			res.json({ success: true, name, token })
		} else {
			res.json({ success: false, error: 'invalid password'  })
		}
	} else {
		res.json({ success: true, name })
	}
})

// TODO: Remove before pushing to production
// Returns a SHA256 hash of given string. Used to make admin pw hash.
// app.get('/hash/:str', (req, res) => {
//	res.send(hash(req.params.str))
// })

// TODO: Move to e.g. socket.js 
// TODO: Store messages in an SQLite file or something? Do we need that?
class Message {
	constructor(user, text, flag) {
	  this.user = user
	  this.text = text
	  this.time = Date.now()
	  this.flag = flag
		this.id = crypto.randomBytes(4).toString('hex') 
	}
	// flags: 0 = normal, 1 = notification
}

const sendMessage = (user, text, flag) => {
	const message = new Message(user, text, flag)
	chatroom.emit('message', message)
	console.log(`(${message.flag}) [${message.time}] ${message.id} - <${message.user}> ${message.text}`)
}

const updateRoom = () => {
	chatroom.emit('chatData', { users: getUsersInChat() })
}

io.of('chat').on('connect', socket => {
	socket.on('join', ({ name, token = null}, callback) => {
		const { error, user } = addUser({ 
			name, 
			id: socket.id, 
			token, 
			ip: socket.request.connection.remoteAddress 
		})

		if(error) return callback(error)

		// TODO: use different room names for different streams
		socket.join('chat') 
		updateRoom()
		sendMessage('admin', `${user.name} joined!`, 1)
		callback()
	})

	socket.on('sendMessage', (text, callback) => {
		const user = getUser(socket.id)
		sendMessage(user.name, text, 0)	
		callback()
	})

	socket.on('deleteMessage', (messageId) => {
		const user = getUser(socket.id)
	  
		// TODO: validate token
		if(user.flag === 1) {
      chatroom.emit('deleteMessage', messageId)
		  console.log(`Admin ${user.name} deleted message ${messageId}`)
		} else {
			console.warn(`Unauthorised user ${user.name} tried to delete a message.`)
		} 
	})

	socket.on('banUser', async(name, reason = 'Did not read the rules.') => {
		if(getUser(socket.id).flag === 1) {
			try {
				const user = getUserByName(name) 
				banUser(user.socketid) 
				chatroom.to(user.socketid).emit('force_leave', reason)
				console.log(`User ${user.name}@${user.ip} banned by admin. Reason: ${reason}`)
			} catch(e) {
				console.error(`Failed to ban user`, e)
			}
		} else {
			console.warn(`Unauthorised user ${user.name} tried to ban someone.`)
		}
	})

	socket.on('disconnect', () => {
		try {
			const user = removeUser(socket.id)
			sendMessage('admin', `${user.name} left.`, 1)
			updateRoom()
		} catch(e) {
			console.log('Failed to remove user', e)
		}
		/* user.disconnected = true // Marks user for deletion, unless they rejoin within 5 seconds.
		setTimeout(() => {
			if(user.disconnected) {
				removeUser(socket.id)
				sendMessage('admin', `${user.name} left.`, 1)
				console.log(`user ${user.id}/${user.name} left`)
			}		
		}, 5000) */	
	})
})

server.listen(process.env.PORT || 8091, () => console.log(`[${process.env.HTTPS ? 'HTTPS' : 'HTTP'}] Chat server started`))
