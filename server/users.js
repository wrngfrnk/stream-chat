
// TODO: This.
class User { 
  constructor(name, socketid, token, flag, ip) {
    this.name = name
		this.socketid = socketid
		this.token = token
		this.flag = flag
		this.ip = ip
  }
}

const users = []
const verifiedUsers = [] 
const bannedUsers = []

// TODO: Some kind of expiration on tokens
const verifyUser = (name, token) => {
	verifiedUsers.push({ name, token }) 
}

const unverifyUser = (name) => {
	const index = verifiedUsers.findIndex(user => user.name === name)
	if(index !== -1) return verifiedUsers.splice(index, 1)[0]
}

const addUser = ({ id, name, token = null, ip }) => {
  name = name.trim()

	const existingUser = users.find(user => user.name.toLowerCase() === name.toLowerCase())	
	const regex = /^[a-zA-ZäöÅÄÖ0-9]+$/

	if(!name || name.length === 0) 
		return { error: 'You need to enter a username!' } 
  
	if(!regex.test(name)) 
		return { error: 'Invalid name! A-Ö and numbers only!' }
	
	if(name.length > 16)  
		return { error: 'Name too long! Max. 16 characters.' }  
	
	if(existingUser)
		return { error: 'Username is already in use!' }
	
	if(bannedUsers.includes(ip)) 
		return { error: 'Banned!' }

	const verified = verifiedUsers.find(user => user.token === token)
	const user = new User(name, id, token, verified ? 1 : 0, ip)

	users.push(user)

	return { user }
}

const removeUser = (id) => {
	const user = getUser(id)
	const index = users.findIndex((user) => user.socketid === id)

	// if(user.flag === 1) unverifyUser(user.name) // TODO: unverify user after a while. 
	if(index !== -1) return users.splice(index, 1)[0]
} 

const getUser = (id) => {
	return users.find(user => user.socketid === id)
}

// TODO: Is this a good idea?
// Used because banUser takes an ID, but I would not like to expose the 
// socketIDs to end users. So, the socket event to ban someone takes
// a name instead, and returns the user with that name. Names are unique,
// so that shouldn't be a problem, right? ...right?
const getUserByName = (name) => {
  return users.find(user => user.name === name)
}

const getUsersInChat = () => {
	return users.map(u => ({ name: u.name, flag: u.flag, ip: u.ip }))
}

const banUser = (id) => {
	const user = getUser(id)
	bannedUsers.push(user.ip)
}

module.exports = { addUser, verifyUser, removeUser, getUser, getUserByName, getUsersInChat, banUser }
